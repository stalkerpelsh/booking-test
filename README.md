#### В проекте использовались

 - create-react-app (react, webpack, etc.)
 - react-google-maps
 - react-tabs
 - react-tag-autocomplete


#### Для установки необходимо запустить

```sh
$ npm install
```

#### Для запуска

```sh
$ npm start
```