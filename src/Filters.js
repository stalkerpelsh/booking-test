import React, {Component} from 'react';
import Data from "./data.json";
import _ from "lodash";
import ReactTags from "react-tag-autocomplete";

class Filters extends Component {
    constructor(props) {
        super(props);
        this.state = {
            checkboxStatus: {
                "Air conditioner": false,
                "Free transportation": false,
                "Hair dryer": false,
                "Wireless internet": false,
                "Computer": false,
                "Free wireless internet": false,
                "Washing machine": false,
                "Elevator": false,
                "Guarded parking": false,
                "Vacuum cleaner": false,
                "TV (local channels only)": false,
                "Balcony": false,
                "Game room": false,
                "Fireplace": false,
                "Terrace": false,
                "Floor heating": false,
                "Blender": false,
                "Dishwasher": false,
                "Coffee maker": false,
                "Freezer": false,
                "Cooking hob": false,
                "Frige": false
            },
            filterList: [],
            filters: Data.optionList,
            searchItems: [],
            tags: [],
            suggestions: []
        }
        this.handleDelete = this.handleDelete.bind(this);
        this.handleAddition = this.handleAddition.bind(this);
    };

    componentWillMount() {
        this.filterInitial();
        this.inputChoiceInitial();
    }


    filterInitial() {
        let optionList = [];

        _.forEach(this.state.filters, function(value, key) {
            let listName = Object.getOwnPropertyNames(value).sort();
            let filterItemsList = [];
            _.forEach(value, function(value, key) {
                let optionsNum = value.length;
                for (let i = 0; i < optionsNum; i++) {
                    let name = value[i].replace(/[ ()]+/g, '');
                    if (i < 6) {
                        filterItemsList.push(
                            <div className="filter__item" key={i}>
                                <label className="filter__label"><input type="checkbox" className="filter__checkbox" value={value[i]} name={name} onChange={this.handleChange.bind(this)}/>
                                    <div className="filter__checkbox-handler"></div>
                                    {value[i]}</label>
                            </div>
                        )
                    } else {
                        filterItemsList.push(
                            <div className="filter__item filter__item_hidden" key={i}>
                                <label className="filter__label"><input type="checkbox" className="filter__checkbox" value={value[i]} name={name} onChange={this.handleChange.bind(this)}/>
                                    <div className="filter__checkbox-handler"></div>
                                    {value[i]}</label>
                            </div>
                        )
                    }

                }
            }.bind(this))

            if (filterItemsList.length < 7) {
                optionList.push(
                    <div className="filter__list" key={key}>
                        <div className="filter__header">
                            <div className="filter__title">{listName}</div>
                        </div>

                        <div className="filter__group">
                            {filterItemsList}
                        </div>
                    </div>
                )
            } else {
                optionList.push(
                    <div className="filter__list" key={key}>
                        <div className="filter__header">
                            <div className="filter__icon"></div>
                            <div className="filter__title" onClick={this.indexCheckboxes.bind(this)}>{listName}</div>
                        </div>

                        <div className="filter__group">
                            {filterItemsList}
                        </div>
                    </div>
                )
            }

        }.bind(this));
        this.setState({filterList: optionList})
    }

    indexCheckboxes(b) {

        let filterGroup = b.target.parentNode.nextSibling;
        let groupIcon = b.target.previousSibling;

        this.setState({
            checkboxCond: !this.state.checkboxCond
        });
        if (this.state.checkboxCond !== true) {
            for (let i = 6; i < filterGroup.childNodes.length; i++) {
                filterGroup.children[i].className = 'filter__item'
            }
            groupIcon.className = 'filter__icon filter__icon_expanded'
        } else {
            for (let i = 6; i < filterGroup.childNodes.length; i++) {
                filterGroup.children[i].className = 'filter__item filter__item_hidden'
            }
            groupIcon.className = 'filter__icon'
        }
    }
    inputChoiceInitial() {
        let itemsAll = []
        let i = 1
        _.forEach(this.state.checkboxStatus, function(value, key) {
            itemsAll.push({
                id: i++,
                name: key
            })
        }.bind(this));

        this.setState({suggestions: itemsAll});

    }

    inputChoice() {
        let itemsChecked = []
        let i = 1
        _.forEach(this.state.checkboxStatus, function(value, key) {
            if (value === true) {
                itemsChecked.push({
                    id: i++,
                    name: key
                })
                let newName = [key].toString().replace(/[ ()]+/g, '');
                let item = document.querySelector("input[name=" + newName + "]");
                item.checked = true;
            }
        }.bind(this));

        this.setState({
            tags: itemsChecked
        }, () => {
            this.props.updateTags(this.state.tags)
        });
    }

    handleChange = (e) => {
        this.setState({
            checkboxStatus: {
                ...this.state.checkboxStatus,
                [e.target.value]: !this.state.checkboxStatus[e.target.value]
            }
        }, () => {
            this.props.updateFilter(this.state.checkboxStatus);
            this.inputChoice();

        });
    }

    handleDelete(i) {
        let tags = this.state.tags.slice(0)
        tags.splice(i, 1)
        this.setState({tags: tags})
        let item = this.state.tags[i];
        let name = _.map({
            item
        }, 'name');
        this.setState({
            checkboxStatus: {
                ...this.state.checkboxStatus,
                [name]: !this.state.checkboxStatus[name]
            }
        }, () => {
            this.props.updateFilter(this.state.checkboxStatus);
            this.inputChoice();
            let newName = [name].toString().replace(/[ ()]+/g, '');
            let item = document.querySelector("input[name=" + newName + "]");
            item.checked = false;
        });
    }
    handleAddition(tag) {
        let tags = this.state.tags.concat(tag)
        this.setState({tags: tags})

        let name = _.map({
            tag
        }, 'name');
        this.setState({
            checkboxStatus: {
                ...this.state.checkboxStatus,
                [name]: !this.state.checkboxStatus[name]
            }
        }, () => {
            this.props.updateTags(this.state.tags);
            this.props.updateFilter(this.state.checkboxStatus);
            this.inputChoice();
            let newName = [name].toString().replace(/[ ()]+/g, '');
            let item = document.querySelector("input[name=" + newName + "]");
            item.checked = true;
        });
    }

    render() {

        return (
            <div className="filter">
                <div className="ammenties">
                    <div className="ammenties__title">Amenities search</div>
                    <ReactTags classNames={{
                        root: 'react-tags ammenties__input',
                        rootFocused: 'is-focused',
                        selected: 'react-tags__selected ammenties__input-selected',
                        selectedTag: 'react-tags__selected-tag ammenties__input-selected-tag',
                        selectedTagName: 'react-tags__selected-tag-name ammenties__input-selected-tag-name',
                        search: 'react-tags__search ammenties__input-search',
                        searchInput: 'react-tags__search-input ammenties__input-search-input',
                        suggestions: 'react-tags__suggestions ammenties__input-suggestions',
                        suggestionActive: 'is-active',
                        suggestionDisabled: 'is-disabled'
                    }} placeholder="enter the names of amenities that you need in apartment" tags={this.state.tags} suggestions={this.state.suggestions} handleDelete={this.handleDelete} handleAddition={this.handleAddition} allowBackspace={false}/>
                </div>
                {this.state.filterList}
            </div>
        )
    }

}
export default Filters;
