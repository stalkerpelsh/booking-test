import React, {Component} from 'react';
import Data from "./data.json";
import Filters from "./Filters";
import Map from "./Map"
import _ from "lodash";
import ReactTags from "react-tag-autocomplete";
import {Tab, Tabs, TabList, TabPanel} from 'react-tabs';

class Apartments extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: Data.apartments,
            apartList: [],
            options: {},
            tags: [],
            itemCount: "",
            suggestions: [],
            filteredPlaces: []
        }
        this.handleDelete = this.handleDelete.bind(this);
        this.handleAddition = this.handleAddition.bind(this);
    };


    componentWillMount() {
        this.apartParse();
        Tabs.setUseDefaultStyles(false);
    }

    handleFilterUpdate(filterValue) {
        this.setState({
            options: filterValue
        }, () => {
            this.apartFilter()
        })
    }

    handleTagsUpdate(tags) {
        this.setState({
            tags: tags
        }, () => {})
    }

    apartFilter() {

        let obj = {}

        _.forEach(this.state.options, function(value, key) {
            if (value === true) {
                obj[key] = value;
            }
        });
        let aptFiltered = _.filter(this.state.data, {options: obj})

        let filteredItems = [];
        for (let i = 0; i < aptFiltered.length; i++) {

            filteredItems.push(
                <div className="apartments__item" key={i}>
                    <div className="apartments__details">
                        <div className="apartments__name">{aptFiltered[i].name}</div>
                        <div className="apartments__price">{aptFiltered[i].price}</div>
                    </div>
                    <img src={require(aptFiltered[i].imgSrc)} alt="" className="apartments__img"/>
                    <div className="apartments__overlay"></div>
                </div>
            )

        }
        this.setState({filteredPlaces: aptFiltered})
        this.setState({itemCount: aptFiltered.length})

        this.setState({apartList: filteredItems})

    }
    apartParse() {
        const apartments = this.state.data;
        let items = [];
        for (let i = 0; i < apartments.length; i++) {

            items.push(
                <div className="apartments__item" key={i}>
                    <div className="apartments__details">
                        <div className="apartments__name">{apartments[i].name}</div>
                        <div className="apartments__price">{apartments[i].price}</div>
                    </div>
                    <img src={require(apartments[i].imgSrc)} alt="" className="apartments__img"/>
                    <div className="apartments__overlay"></div>
                </div>
            )

        }
        this.setState({filteredPlaces: apartments})

        this.setState({itemCount: apartments.length})
        this.setState({apartList: items})
    }

    handleDelete(i) {
        let tags = this.state.tags.slice(0)
        tags.splice(i, 1)
        this.setState({tags: tags})
    }
    handleAddition(tag) {
        let tags = this.state.tags.concat(tag)
        this.setState({tags: tags})
    }

    render() {

        return (
            <div className="apartments">
                <header className="header">
                    <a href="#" className="header__logo"><img src={require("./assets/img/logo.png")} alt="" className="header__logo-img"/></a>
                    <a href="#" className="header__button">List your property</a>
                </header>
                <div className="apartments__search">
                    <div className="apartments__title">
                        <div className="filter__icon filter__icon_expanded"></div>
                        <span className="filter__title">Amenties</span>
                    </div>
                    <Filters updateTags={this.handleTagsUpdate.bind(this)} updateFilter={this.handleFilterUpdate.bind(this)} tagProps={this.state.tags}/>
                </div>
                <div className="apartments__result">

                    <Tabs onSelect={this.handleSelect}>
                        <TabList>
                            <Tab>Map</Tab>
                            <Tab>Accomodations</Tab>
                        </TabList>

                        <TabPanel>
                            <div className="apartments__counter">
                            <div className="apartments__counter-value">{this.state.itemCount} offers</div>
                            </div>
                            <Map markers={this.state.filteredPlaces}/>
                        </TabPanel>
                        <TabPanel>
                            <div className="apartments__counter">
                                <ReactTags classNames={{
                                    root: 'react-tags apartments__result-tags',
                                    rootFocused: 'is-focused',
                                    selected: 'react-tags__selected ammenties__input-selected',
                                    selectedTag: 'react-tags__selected-tag ammenties__input-selected-tag',
                                    selectedTagName: 'react-tags__selected-tag-name ammenties__input-selected-tag-name',
                                    search: 'react-tags__search apartments__result-search',
                                    searchInput: 'react-tags__search-input ammenties__input-search-input',
                                    suggestions: 'react-tags__suggestions ammenties__input-suggestions',
                                    suggestionActive: 'is-active',
                                    suggestionDisabled: 'is-disabled'
                                }} placeholder="enter the names of amenities that you need in apartment" tags={this.state.tags} suggestions={this.state.suggestions} handleDelete={this.handleDelete} handleAddition={this.handleAddition}/>
                            <div className="apartments__counter-value">{this.state.itemCount} offers</div>
                            </div>
                            <div className="apartments__list">
                                {this.state.apartList}
                            </div>
                        </TabPanel>
                    </Tabs>

                </div>
            </div>
        );

    }
}

export default Apartments;
