import _ from "lodash";

import {default as React, Component, PropTypes} from "react";

import withScriptjs from "react-google-maps/lib/async/withScriptjs";

import {withGoogleMap, GoogleMap, Marker} from "./lib";

import MarkerClusterer from "./lib/addons/MarkerClusterer";

import MapStyle from "./mapStyle.json";

const markerPic = require("./assets/img/icon-marker.png");
const clusterPic = require("./assets/img/icon-cluster.png");

const MarkerClustererExampleGoogleMap = _.flowRight(withScriptjs, withGoogleMap,)(props => (
    <GoogleMap defaultZoom={14} defaultCenter={{
        lat: 41.393232,
        lng: 2.164321
    }} defaultOptions={{
        styles: MapStyle,
        disableDefaultUI: true
    }}>
        <MarkerClusterer  averageCenter  enableRetinaIcons gridSize={60} >
            {props.markers.map(marker => (<Marker icon={{
                url: markerPic
            }} position={{
                lat: marker.latitude,
                lng: marker.longitude
            }} key={marker.price} label={{
                text: "€" + marker.price,
                color: "#fff",
                fontSize: "13",
                fontFamily: "Open Sans",
                fontWeight: "600"
            }}/>))}
        </MarkerClusterer>
    </GoogleMap>
));


class Map extends Component {

    state = {
        markers: []
    }

    componentWillReceiveProps(nextProps) {
        this.setState({markers: nextProps.markers});
    }

    componentDidMount() {
        this.setState({markers: this.props.markers});
    }

    render() {
        return (
            <MarkerClustererExampleGoogleMap googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=AIzaSyB1GKqcqBz-yf4Ab_piYz5k4ODBLF0BCzo" loadingElement={< div style = {{ height: `100%` }} > </div>} containerElement={< div style = {{ height: `100%` }}/>} mapElement={< div style = {{ height: `100%` }}/>} markers={this.state.markers}/>
        );
    }
}
export default Map;
