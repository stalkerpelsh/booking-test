import React, { Component } from 'react';
import './assets/css/style.css'
import Apartments from './Apartments';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Apartments />
      </div>
    );
  }
}

export default App;
